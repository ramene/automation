OMA NONPROD

OPSMAN - 1.10.9
ERT - 1.10.14-build2
JMX - 1.8.22
Redis - 1.9.0
Metrics - 1.3.7
MySQL - 2.0.2


OMA ENG

OPSMAN - 1.9.4.0
ERT - 1.10.14-build2
JMX - 1.8.22
Redis - 1.9.0
Metrics - 1.3.7
MySQL - 2.0.2
APPDYNAMICS
SCS